<?php

use Illuminate\Database\Seeder;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $users = [
            [
                'first_name' => 'Jose',
                'last_name' => 'Pineda',
                'type_dni' => 'Cedula',
                'dni' => '27.550.155',
                'password' => bcrypt('4978430xd'),
                'type_id' => 1,
                'is_active' => true,
                'email' => 'jose@pineda.website',
            ],
        ];

        DB::table('users')->insert($users);
    }
}
