<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePaymentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('payments', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->float('amount_payed');
            $table->unsignedBigInteger('bank');
            $table->unsignedBigInteger('currency');
            $table->unsignedBigInteger('id_method');
            $table->unsignedBigInteger('status');
            $table->unsignedBigInteger('invoice_id');
            $table->string('transaction_ref', 200);

            $table->foreign('id_method')->references('id')->on('payment_methods')->onDelete('cascade');
            $table->foreign('currency')->references('id')
                ->on('currencies')->onDelete('cascade');
            $table->foreign('invoice_id')->references('id')
                ->on('invoices')->onDelete('cascade');
            $table->foreign('status')->references('id')
                ->on('status')->onDelete('cascade');
            $table->foreign('bank')->references('id')->on('banks')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('payments');
    }
}
