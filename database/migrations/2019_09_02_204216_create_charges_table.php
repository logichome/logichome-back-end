<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateChargesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('charges', function (Blueprint $table) {
            $table->bigIncrements('id')->unsigned();
            $table->string('invoice_id',300);
            $table->string('name', 100);
            $table->BigInteger('amount');
            $table->json('options')->nullable();
            $table->string('reason',300)->nullable();
            $table->date('spend_date');
            $table->unsignedBigInteger('type');

            $table->foreign('invoice_id')->references('id')
                ->on('invoices')->onDelete('cascade');
            $table->foreign('type')->references('id')
                ->on('charges_types')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('charges');
    }
}
