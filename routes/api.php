<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

# Routes for forget password and password recovery
Route::post('forget-password', 'Auth\AuthController@forget');
Route::post('reset-password', 'Auth\AuthController@resetPassword');

# Group routes for the Auth Model methods.
Route::prefix('auth')->group(function () {
    Route::post('login', 'Auth\AuthController@login');
    Route::post('register', 'Auth\AuthController@register');
    Route::post('logout', 'Auth\AuthController@logout');
});

# Group routes for the resource methods models
Route::prefix('')->group(function () {
    Route::apiResource('address', 'Geolocation\GeolocationController');
    Route::apiResource('users', 'User\UserController');
    Route::apiResource('residence', 'Residence\ResidenceController');
    Route::apiResource('property', 'Property\PropertyController');
    Route::apiResource('invoice', 'Invoice\InvoiceController');
    Route::apiResource('charge', 'Charge\ChargeController');
    Route::apiResource('payment', 'Payment\PaymentController');
    Route::get('property-user/{id}', 'Property\PropertyController@showThroughUser');
    // Return a property with their payments
    Route::get('property-payment/{id}', 'Payment\PaymentController@showThroughProperty');
    // Make new residence with address
    Route::post('new-residence', 'Residence\ResidenceController@createResidenceWithAddress');
    // Return all residences of an Auditor
    Route::get('residence-auditor/{id}', 'Residence\ResidenceController@getResidencesThroughUser');
    // Return all properties in a Residence
    Route::get('property-residence/{id}', 'Property\PropertyController@showThroughResidence');
    // Return the last invoice of a residence
    Route::get('last-invoice/{id}', 'Invoice\InvoiceController@getLastOne');
    // Return all invoices in a residence
    Route::get('invoice-residence/{id}', 'Invoice\InvoiceController@showThroughResidence');
    // Return inactive invoices
    Route::get('inactive-invoices/', 'Invoice\InvoiceController@InactiveInvoices');
    // Return all charges in a invoice
    Route::get('charge-invoice/{id}', 'Charge\ChargeController@showThroughInvoice');
    // Return all payments in a invoice
    Route::get('payment-invoice/{id}', 'Payment\PaymentController@showThroughInvoice');
    // Get all the countries
    Route::get('countries', 'Geolocation\GeolocationController@countries');
    // Get all the states
    Route::get('states', 'Geolocation\GeolocationController@states');
    // Get all cities
    Route::get('cities', 'Geolocation\GeolocationController@cities');
    // Get all payments in a community
    Route::get('payment-residence/{id}', 'Residence\ResidenceController@getPaymentThroughResidence');
    // Get the defaulters of a community
    Route::get('defaulters/{id}', 'Property\PropertyController@getDefaulters');
    // Get the properties who has paid, but hasn't confirmed the invoice
    Route::get('confirm-payments/{id}', 'Payment\PaymentController@getPropertiesPayed');
    // Get the unpaid invoices from a property
    Route::get('inactive-payments-property/{id}', 'Property\PropertyController@getInactivePaymentsFromProperty');
    // Get types of residences from residences_type
    Route::get('residences-types/', 'Residence\ResidenceController@getResidenceType');
    // Get all banks in the DB
    Route::get('banks/', 'Payment\PaymentController@getBanks');
    // Get all methods in the DB
    Route::get('methods/', 'Payment\PaymentController@getMethodsPayments');
    // Get the types of charges
    Route::get('charges-types/', 'Charge\ChargeController@getChargesTypes');
    // Get all the currencies
    Route::get('currencies/', 'Invoice\InvoiceController@Currencies');
    // Is a function sum which work as "ProntoPago"
    Route::post('prontopago/{id}', 'Charge\ChargeController@prontoPago');
    // Get status of pay.
    Route::get('status-pay/{id}/{paymentStatus}', 'Invoice\InvoiceController@paymentStatus');
    // Pay all the invoices
    Route::post('pay-invoices/{id}', 'Payment\PaymentController@getDebtFromProperty');
    // Pay all the invoices
    Route::post('proof/{id}', 'Payment\PaymentController@proof');
});


