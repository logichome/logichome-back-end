<?php

namespace App\Models;

use App\Models\Charge;
use App\Models\Property;
use Illuminate\Database\Eloquent\Model;

class Payment extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'amount_payed', 'id_method', 'bank',
        'is_payed', 'is_completed', 'is_confirmed', 'transaction_ref',
        'invoice_id', 'property_id', 'currency'
     ];

    public $timestamps = false;

     /**
      * The attributes that should be hidden for arrays.
      *
      * @var array
      */
     protected $hidden = [
        'created_at', 'updated_at'
     ];

   /**
   * Get the property record associated with the payments.
   */
   public function property()
   {
       return $this->belongsToMany(Property::class, 'payment_property_invoice');
   }

    public function invoice()
    {
        return $this->belongsToMany(Invoice::class, 'payment_property_invoice');
    }

    public function bank() {
       return $this->belongsTo(Bank::class, 'bank', 'id');
    }

    public function method() {
        return $this->belongsTo(Methods::class, 'id_method');
    }
}

