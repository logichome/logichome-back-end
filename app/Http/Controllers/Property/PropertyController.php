<?php

namespace App\Http\Controllers\Property;

use App\Models\Payment;
use App\Models\Property;
use App\Models\Residence;
use App\Helpers\ApiHelpers;
use App\Models\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class PropertyController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function index()
    {
        $size = empty($request['paginate']) ? 1000 : $request['paginate'];
        $index = Property::with(['residences','user'])->paginate((int)$size);

        return ApiHelpers::ApiResponse(200, 'Successfully completed', $index);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function store(Request $request)
    {
        $find = User::where('email', $request->user_email)->firstOrFail()->id;
        $store = Property::create([
            'user_id' => $find,
            'reference' => $request->reference,
            'alicuota' => $request->alicuota,
            'residences_id' => $request->residences_id,
        ]);

        return ApiHelpers::ApiResponse(200, 'Successfully completed', $store);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Property  $property
     * @return \Illuminate\Http\JsonResponse
     */
    public function show(Property $property)
    {
        $show = Property::findOrFail($property->id);
        $show->residences;
        $show->user;

        return ApiHelpers::ApiResponse(200, 'Successfully completed', $show);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Property  $property
     * @return \Illuminate\Http\JsonResponse
     */
    public function update(Request $request, Property $property)
    {
        $update = Property::findOrFail($property->id);
        $update->update($request->all());

        return ApiHelpers::ApiResponse(200, 'Successfully completed', $update);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Property  $property
     * @return \Illuminate\Http\Response
     */
    public function destroy(Property $property)
    {
        $delete = Property::findOrFail($property->id);
        $delete->delete();

        return ApiHelpers::ApiResponse(200, 'Property deleted', null);
    }

    public function showThroughUser($id)
    {
        $user = User::findOrFail($id);
        $user->property;
        $residences_id = [];
        $residences = [];
        foreach ($user->property as $key => $value) {
            array_push($residences_id, $value['residences_id']);
        }
        // Order
        sort($residences_id);

        foreach ($residences_id as $key => $value) {
            if ($key + 1 == count($residences_id)){
                break;
            } else {
                if ($value != $residences_id[$key + 1]) {
                    $residence = Residence::findOrFail($value);
                    array_push($residences, $residence);
                }
            }
        }
        $query = Residence::findOrFail(end($residences_id));
        array_push($residences, $query);

        if ($query) {
            return ApiHelpers::ApiResponse(200, 'Succesfully Complete', [$user, 'residences' => $residences]);
        } else {
            return ApiHelpers::ApiResponse(404, 'Succesfully Complete', false);
        }
    }

    public function showThroughResidence($id) {
        $properties = Property::with('user')->where('residences_id', $id)->get();
        // $properties->user;

        if ($properties->all() == null) {
            return ApiHelpers::ApiResponse(404, '404 not found', null);
        } else {
            return ApiHelpers::ApiResponse(200, 'Succesfully completed', $properties);
        }
    }


    public function getDefaulters($id) {
        //get properties
        $residences = Residence::with('property')->findOrFail($id);
        $properties = [];
        // push properties in an array
        foreach ($residences->property as $propertyID) {
            $propertyID->total = $propertyID->credit - abs($propertyID->debt);
            array_push($properties, $propertyID);
        }

        // return a true response
        return ApiHelpers::ApiResponse(200, 'Succesfully completed', $properties);
    }

    public function getInactivePaymentsFromProperty($id) {
        $propertiesWithPayments = Payment::with(['property', 'invoice'])->whereHas('property', function ($query) use ($id) {
            $query->where('property_id', $id);
        })
            ->where(function ($q) {
                $q->where('is_payed', 0)->where('is_payed', 0)->orWhere('is_completed', 0);
            })
            ->get();

        // return the response
        if ($propertiesWithPayments->all() == null) {
            return ApiHelpers::ApiResponse(404, '404 not found', null);
        } else {
            foreach ($propertiesWithPayments as $i => $info) {
                $info->total_payed = ($info->invoice[0]->total * $info->property[0]->alicuota) / 100;
                if ($info->invoice[0]->is_active == true) {
                    unset($propertiesWithPayments[$i]);
                }
            }
            return ApiHelpers::ApiResponse(200, 'Succesfully completed', $propertiesWithPayments);
        }
    }
}
