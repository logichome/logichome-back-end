<?php

namespace App\Http\Controllers\Invoice;

use App\Helpers\ApiHelpers;
use App\Models\Invoice;
use App\Models\Payment;
use App\Models\Property;
use App\models\Property_Payment;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use GuzzleHttp\Client;

class InvoiceController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function index()
    {
        $index = Invoice::with('residence')->paginate(200);
        return ApiHelpers::ApiResponse(200, 'Successfully completed', $index);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function store(Request $request)
    {
        $query = Invoice::with('charges')->where('residence_id', $request->residence_id)->latest('id')->first();
        $client = new Client([
            'base_uri' => 'https://s3.amazonaws.com'
        ]);

        $res = $client->request('GET', 'dolartoday/data.json');
        $sicad = json_decode(mb_convert_encoding($res->getBody()->getContents(), 'UTF-8', 'UTF-8'))->USD->sicad2;
        $dolarPrice = $sicad == null ? json_decode(mb_convert_encoding($res->getBody()->getContents(), 'UTF-8', 'UTF-8'))->USD->transferencia : $sicad;

        if ($query != null) {
            $query->is_active = false;
            $query->save();
        }

        $store = Invoice::create($request->all());
        return ApiHelpers::ApiResponse(200, 'Succesfully completed', $store);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function show($id)
    {
        $invoice = Invoice::with(['currency'])->findOrFail($id);
        $invoice->charges;
        $invoice->residence;
        $client = new Client([
            'base_uri' => 'https://s3.amazonaws.com'
        ]);

        $res = $client->request('GET', 'dolartoday/data.json');
        $sicad = json_decode(mb_convert_encoding($res->getBody()->getContents(), 'UTF-8', 'UTF-8'))->USD->sicad2;
        $invoice->price = $sicad == null ? json_decode(mb_convert_encoding($res->getBody()->getContents(), 'UTF-8', 'UTF-8'))->USD->transferencia : $sicad;

        return ApiHelpers::ApiResponse(200, 'Succesfully completed', $invoice);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function update(Request $request, $id)
    {
        $invoice = Invoice::findOrFail($id);

        $invoice->update($request->all());

        return ApiHelpers::ApiResponse(200, 'Succesfully completed', $invoice);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function destroy($id)
    {
        $invoice = Invoice::findOrfail($id);
        $invoice->delete();

        return ApiHelpers::ApiResponse(200, 'Succesfully completed', null);
    }

    public function showThroughResidence($id) {
        $invoice = Invoice::with(['charges', 'residence'])->where('residence_id', $id)
            ->where('is_active', 0)
            ->orderBy('id', 'desc')->get();
        if ($invoice->all() == null) {
            return ApiHelpers::ApiResponse(404, '404 not found', null);
        } else {
            return ApiHelpers::ApiResponse(200, 'Succesfully completed', $invoice);
        }
    }

    public function getLastOne($id) {
        $invoice = Invoice::with(['charges', 'residence'])->where('residence_id', $id)->get()->last();
        if ($invoice == null) {
            return ApiHelpers::ApiResponse(404, '404 not found', null);
        } else {
            return ApiHelpers::ApiResponse(200, 'Succesfully completed', $invoice);
        }
    }

    public function InactiveInvoices() {
        $invoices = Invoice::with('residence')->where('payed', 0)->paginate(20);

        return ApiHelpers::ApiResponse(200, 'Succesfully completed', $invoices);
    }

    public function Currencies() {
        $currencies = DB::table('currencies')->get();

        return ApiHelpers::ApiResponse(200, 'Succesfully completed', $currencies);
    }

    public function paymentStatus($id, $propertyId) {
        $invoice = DB::table('payment_property_invoice')->where('invoice_id', $id)
            ->where('property_id', $propertyId)->get();
        $payment = Payment::findOrFail($invoice[0]->payment_id);
        return ApiHelpers::ApiResponse(200, 'Succesfully completed', $payment);
    }
}
