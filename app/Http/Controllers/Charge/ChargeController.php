<?php

namespace App\Http\Controllers\Charge;

use App\Helpers\ApiHelpers;
use App\Models\Charge;
use App\Models\Invoice;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;

class ChargeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function index()
    {
        $index = Charge::with('invoices')->paginate(200);

        return ApiHelpers::ApiResponse(200, 'Successfully completed', $index);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function store(Request $request)
    {
        $store = Charge::create($request->all());
        $invoice = Invoice::findOrFail($request->invoice);
        $op = $invoice->total + $request->amount;
        $invoice->total = $op;
        $invoice->save();

        return ApiHelpers::ApiResponse(200, 'Successfully completed', $store);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function show($id)
    {
        $show = Charge::with('invoices')->findOrFail($id);

        return ApiHelpers::ApiResponse(200, 'Successfully completed', $show);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function update(Request $request, $id)
    {
        $charge = Charge::findOrFail($id);

        $charge->update($request->all());

        return ApiHelpers::ApiResponse(200, 'Succesfully completed', $charge);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function destroy($id)
    {
        $charge = Charge::findOrFail($id);
        $charge->delete();

        return ApiHelpers::ApiResponse(200, 'Successfully completed', null);
    }

    public function showThroughInvoice($id) {
        $charges = DB::table('charges')->where('invoice', $id)->paginate(50);

        if ($charges->all() == null) {
            $response = ApiHelpers::ApiResponse(404, '404 not found', null);
        } else {
            $response = ApiHelpers::ApiResponse(200, 'Succesfully completed', $charges);
        }
        return $response;

    }

    public function getChargesTypes() {
        $types = DB::table('charges_types')->get();

        return ApiHelpers::ApiResponse(200, 'Succesfully completed', $types);
    }

    public function prontoPago(Request $request, $id) {

        $invoice = Invoice::findOrfail($id);
        $response = '';
        $code = 0;

        if ($invoice->prontopago == false) {
            $total = $invoice->total + $request->amount;
            $invoice->total = $total;
            $invoice->prontopago = true;
            $store = Charge::create($request->all());
            $response = 'Succesfully completed';
            $code = 200;
            $invoice->save();
        }
        else {
            $response = 'Prontopago is already added';
            $code = 500;
        }

        return ApiHelpers::ApiResponse($code, $response, $invoice);;
    }
}
