<?php

namespace App\Http\Controllers\Payment;

use App\Helpers\ApiHelpers;
use App\Models\Invoice;
use App\Models\Payment;
use App\Models\Property;
use App\models\Property_Payment;
use App\Models\Residence;
use GuzzleHttp\Client;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use function foo\func;

class PaymentController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return JsonResponse
     */
    public function index()
    {
        $index = Payment::with(['property', 'invoice'])->paginate();

        return ApiHelpers::ApiResponse(200, 'Succesfully completed', $index);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param Request $request
     * @return JsonResponse
     */
    public function store(Request $request)
    {
        $data = [
            'amount_payed' =>    $request->amount_payed,
            'transaction_ref' => $request->transaction_ref,
            'id_method' =>       $request->id_method,
            'bank' =>            $request->bank,
        ];

        $store = Payment::create($data);

        return ApiHelpers::ApiResponse(200, 'Succesfully completed', $store);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return JsonResponse
     */
    public function show($id)
    {
        $show = Payment::with(['property', 'invoice'])->findOrFail($id);

        return ApiHelpers::ApiResponse(200, 'Successfully completed', $show);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param Request $request
     * @param  int  $id
     * @return JsonResponse
     */
    public function update(Request $request, $id)
    {
        $queryPayment = Payment::with(['invoice', 'property'])->findOrFail($id);

        if ((($queryPayment->invoice[0]->total *  $queryPayment->property[0]->alicuota) / 100) > $request->amount_payed) {
            $queryPayment->update($request->all());
        }
        if ((($queryPayment->invoice[0]->total *  $queryPayment->property[0]->alicuota) / 100) > $queryPayment->amount_payed) {
            $queryPayment->update($request->all());
        } if((($queryPayment->invoice[0]->total *  $queryPayment->property[0]->alicuota) / 100) <= $queryPayment->amount_payed) {
            $queryPayment->update($request->all());
            $queryPayment->is_completed = 1;
            $queryPayment->is_payed = 1;
            $queryPayment->save();
        }

        return ApiHelpers::ApiResponse(200, 'Successfully completed', $queryPayment);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return JsonResponse
     */
    public function destroy($id)
    {
        $destroy = Payment::findOrFail($id);
        $destroy->delete();

        return ApiHelpers::ApiResponse(200, 'Successfully completed', null);
    }

    public function showThroughInvoice($id) {
        $payment = Invoice::with('payments')->where('id', $id)->paginate(50);

        if ($payment->all() == null) {
            return ApiHelpers::ApiResponse(404, '404 not found', null);
        } else {
            return ApiHelpers::ApiResponse(200, 'Successfully completed', $payment);
        }
    }

    public function showThroughProperty($id) {
        $payment = Property::with('payment')->where('id', $id)->paginate(50);

        if ($payment->all() == null) {
            return ApiHelpers::ApiResponse(404, '404 not found', null);
        } else {
            return ApiHelpers::ApiResponse(200, 'Successfully completed', $payment);
        }
    }

    public function getBanks() {
        $banks = DB::table('banks')->get();

        return ApiHelpers::ApiResponse(200, 'Successfully completed', $banks);
    }

    public function getMethodsPayments() {
        $methods = DB::table('payment_methods')->get();

        return ApiHelpers::ApiResponse(200, 'Successfully completed', $methods);
    }

    public function getPropertiesPayed($id) {
        $residences = Residence::with('property')->findOrFail($id);
        $properties = [];
        foreach ($residences->property as $propertyID) {
            array_push($properties, $propertyID['id']);
        }
        $propertiesWithPayments = Payment::with(['property', 'invoice', 'bank', 'method'])->whereHas('property', function ($query) use ($properties) {
            $query->whereIn('property_id', $properties);
        })
            ->where('is_payed', 1)
            ->where(function($q) {
                $q->where('is_confirmed', 0)->orWhere('is_completed', 0);
            })
            ->get();

        // return the response
        if ($propertiesWithPayments->all() == null) {
            return ApiHelpers::ApiResponse(404, '404 not found', null);
        } else {
            return ApiHelpers::ApiResponse(200, 'Succesfully completed', $propertiesWithPayments);
        }
    }

    public function getDebtFromProperty(Request $request, $id) {
        $property = Property::with(['payment' => function($q) {
            $q->where('is_payed', false);
        }])->findOrFail($id);

        return ApiHelpers::ApiResponse(200, 'Succesfully completed', $property);
    }

    public function proof(Request $request, $id)
    {
        // Get dolar price
       /* $client = new Client([
            'base_uri' => 'https://s3.amazonaws.com'
        ]);

        $res = $client->request('GET', 'dolartoday/data.json');
        $sicad = json_decode(mb_convert_encoding($res->getBody()->getContents(), 'UTF-8', 'UTF-8'))->USD->sicad2;*/
       $sicad = 197893.71;

        // making the query
        $property = Property::with(['invoice' => function($q) {
            $q->where('is_active', false);
        }])->findOrFail($id);

        // Data Filter
        if ($request->currency == 1) {
            $amount=number_format((float)abs( $property->credit + $request->amount_payed), 2, '.', '');
        }
        if ($request->currency == 2) {
            $amount=number_format((float)abs( $property->credit + ($request->amount_payed) / $sicad), 2, '.', '');
        }
        $debt = number_format((float)abs($property->debt), '2', '.', '');
        $saldo = $amount - $debt;

        // Operation

        $return = [$debt, $saldo, $amount, $property];

        return ApiHelpers::ApiResponse(200, 'Succesfully completed', $return);
    }
}
